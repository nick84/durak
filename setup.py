from cx_Freeze import setup, Executable

target = Executable(
    script = "./src/durak.py",
    icon = "./src/icon/icon.ico")

excludes = [
    'bz2',
    'email',
    'html',
    'http',
    'logging',
    'lzma',
    'pyexpat',
    'select',
    'socket',
    'ssl',
    'unicodedata',
    'unittest',
    'urllib',
    'xml',
]

zip_include_packages = [
    "collections",
    "encodings",
    "hashlib",
    "importlib",
]

options = {
    "build_exe":{
            "excludes":excludes,
            "zip_include_packages":zip_include_packages,
        }
}

setup(
    name = "durak",
    version = "0.1",
    description = "Durack",
    author = "Denis",
    options = options,
    executables = [target]
)