﻿#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# класс "Карта"
class Card:
    # card - карта, suit - масть
    def __init__(self, card, suit):
        self.card, self.suit = card, suit
    
    def __repr__(self):
        return "{card}{suit}".format(card = self.card, suit=self.suit)


from random import randrange as rr
# класс "КолодаКарт"
class CardsDeck:
    def __init__(self):
        self.init_deck()
    
    # создание упорядочненой(по значению т.е. числу) колоды карт от 6 до 14,
    # где 11 - валет, 12 - дама, 13 - кароль, 14 - туз
    def init_deck(self):
        self.deck = []
        # создание колоды карт отсортированной по значению (т.е. числу)
        # for card in range(6,15):
            # for suit in ["♥","♦","♠","♣"]:
                # self.deck.append(Card(card,suit))
        # создание колоды карт отсортированной по масти(где каждая масть отсортирована по значению)
        for suit in ["♥","♦","♠","♣"]:
            for card in range(6,15):
                self.deck.append(Card(card,suit))
    
    # перемешать карты (один раз)
    def shuffle(self):
        poped_cards = []
        # извлеч рандомно половину карт из колоды
        for _ in range(int(len(self.deck)/2)):
            poped_cards.append(self.deck.pop(rr(0,len(self.deck))))
        # вернуть извлученую половину карт в колоду,
        # но в рандомном порядке
        for card in poped_cards:
            # на каждой итерации для rr проверяем длину колоды
            # потому как длина колоды пополняется на +1
            self.deck.insert(rr(0,len(self.deck)),card)
        # или использовать random.shuffle :D
        # import random
        # random.shuffle(self.deck)
    
    def __repr__(self):
        return "CardsDeck("+str(self.deck)+")"
    
    # получить текущую козырную масть
    # при первом обращение из колоды карт
    # извлекается рандомная карта которая и является козырем
    def trump_suit(self):
        return self.trump_card().suit
    
    # получить текущую козырную карту
    def trump_card(self):
        if hasattr(self,"_CardsDeck__tcard"):
            return self.__tcard
        self.__tcard = self.deck.pop(rr(0,len(self.deck)))
        self.deck.insert(0,self.__tcard)
        return self.__tcard
    
    def pop(self):
        return self.deck.pop()
        
class User:
    def __init__(self, card, name="User"):
        self.cards = list([card])
        self.__name = name
    
    def take_card_to_user(self, card):
        self.cards.append(card)
    
    def suits(self, suit):
        return [_suit for _suit in self.cards if _suit.suit == suit]
    
    def __repr__(self):
        return self.__name+"("+str(self.cards)+")"
    
    def __iter__(self):
        for item in self.cards:
            yield item
    
    def __getitem__(self, index):
        return self.cards[index]


#сортировка пользователей (относительно козырных карт на руках)
def sort_cards(suit):
    # приоритет считается по максимальной козырной карте на руках
    # если нужно считать по сумме значений козырных карт на руках,
    # то переменной cards_max присваивать сумма значений козырных карт
    def max_card(cards):
        cards_max = 0
        for c in cards:
            # при сумме значений козырных карт использовать этот "if"
            # if c.suit == suit:
                # cards_max += c.card
            
            # при максимальной козырной карте использовать этот "if"
            if c.suit == suit and cards_max < c.card:
                cards_max = c.card
        return cards_max
    return max_card

if __name__ == "__main__":
    users_count = 0
    import sys
    while not (2 <= users_count <= 6):
        try:
            users_count = int(sys.argv[1])\
                            if len(sys.argv) == 2 and sys.argv[1].isdigit() and 2 <= int(sys.argv[1]) <= 6\
                            else input("Введите количество игроков от 2 до 6 или 'q' для выхода: ")
            if users_count == "q":
                import sys
                sys.exit(0)
            users_count = int(users_count)
            if 2 <= users_count <= 6:
                break
        # замечание, если не писать "except Exception as _:",
        # а просто написать "except:" то не работает ни sys.exit ни просто exit или quit
        except Exception as _:
            users_count = 0
    cardsDeck = CardsDeck()
    # перетасовать 50 раз
    for _ in range(50):
        cardsDeck.shuffle()
    print("\nКозырь '%s':\n" % cardsDeck.trump_suit())
    users = []
    # создание пользователей и раздача им карт (каждому игроку 6 карт за раз)
    for user in range(users_count):
        users.append(User(cardsDeck.deck.pop(),"User%d"%(user+1)))
        for _ in range(5):
            users[user].take_card_to_user(cardsDeck.pop())
    print("Оставшиеся карты в колоде:\n");print(cardsDeck)
    print("\nКарты на руках:\n")
    for user in range(users_count):
        #print("User%d %s" % (user+1, users[user].cards))
        print(users[user])
    # сортировка и вывод пользователей по убыванию(относительно козырных карт на руках)
    print("\nОтсортированный по убыванию список пользователей с козырями на руках:")
    scards = sort_cards(cardsDeck.trump_suit())
    for suser in sorted(users,key=scards,reverse=True):
        print("-\n%s"%suser)
        print(suser.suits(cardsDeck.trump_suit()))
    import os
    os.system("pause")